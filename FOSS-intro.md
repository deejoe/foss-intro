
[Open source](https://opensource.org/osd-annotated) is part of a broader movement to bring the benefits of software freedom to everyone who uses a computer. 

[Software freedom](https://www.gnu.org/philosophy/free-sw.html), in its simplest expression, means that a piece of software is available for everyone, either directly or through help from their choice of experts, to exercise all of the following four freedoms:

  * The freedom to use the software for any purpose.
  * The freedom to read the software's source code.
  * The freedom to modify that source code.
  * The freedom to share the source code, in either its original form or with any modifications that have been made to it.

As with many other freedoms, the benefits of software freedom are difficult to list fully, but the history of their recognition and promotion owes much to the concepts of academic freedom and peer review. The benefits are similarly wide-reaching and grow as our use of computing itself grows. Software freedom enables communities of practice to build around free software codebases for critiquing, improving, and extending our collective computing capabilities and all the ways in which we use those capabilities.

The movement to promote software freedom is one of the earliest and best-established components of open scholarship. It has, in turn, inspired and informed other open scholarship efforts, such as open access publishing, the use and production of open education resources, and work to improve research reproducibility.

As with any movement, there is a diversity of opinion around what methods to use and what goals to prioritize. Aspects of software freedom are pursued, variously, as [free software](https://www.fsf.org/), as [open source](https://opensource.org/), or much more recently, as 
[public software](https://publicsoftware.eu/about/why-public-software/) or
[public code](https://standard.publiccode.net/). 


*Copyright 2019 D. Joe Anderson. Available for re-use under the terms of the 
Creative Commons Attribution-ShareAlike 4.0 International license 
[(CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/legalcode)*

